<?php
//Send an email to registrant and camp directors

print_r($email);

        $to = $email;

		$subject = "Myers Running Camp Registration";
		$message   = "Hi, " . $firstName . " " . $lastName .",\n\nThank you for registering for the 2016 Myers Running Camp.\n\nYou have decided to attend a camp in which we feel very strongly about. It is our goals to give you a chance to hear from some world class speakers in their respective fields, demonstrate some training drills and techniques that are currently used by professional runners, and create an atmosphere where you will meet new people and go home with some new great friends.\n\nWe will email you directions to the camp, and a list of things to bring about two weeks prior to camp. If you need to withdraw your registration, please keep in mind that you must do so BEFORE 3 weeks prior to the camp in order to get a refund minus a $50.00 processing fee. This is a pretty strict policy in order to ensure enough spots at the campgrounds.\n\nIf you need to get in touch with us, please call Chad Myers at 614-581-2191. If you are paying with a check, please make it out to Myers Running Camp, and mail to: Myers Running Camp, 6595 Lake Rd. Pleasantville, OH 43148.\n\n";
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		// Additional headers
		$headers .= 'To: ' . $to . "\r\n";
		$headers .= 'From: chadmyers262@gmail.com' . "\r\n";
		$headers .= 'Cc: chadmyers262@gmail.com' . "\r\n";
				
        mail($to, $subject, $message, $headers);
?>