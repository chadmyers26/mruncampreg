<?php
include 'db.php';
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$app->get('/campers','getCampers');
$app->get('/campers/:id','getCamperById');
$app->delete('/campers/delete/:id','deleteCamper');
// $app->edit('/campers/edit/:id','editCamper');
// $app->get('/admin','getAdmin');


// $app->get('/updates','getUserUpdates');
// $app->post('/updates', 'insertUpdate');
// $app->delete('/updates/delete/:update_id','deleteUpdate');
// $app->get('/campers/search/:query','getCamperSearch');

$app->run();

// $errors = array();
// $data = array();

// //Do validation on the form data
// if (empty($_POST['lastName']))
//   $errors['lastName'] = 'last name is required.';



// if ( ! empty($errors)) {

// 	// if there are items in our errors array, return those errors
// 	$data['success'] = false;
// 	$data['errors']  = $errors;
// 	echo json_encode($data);

// } else {

// 	// if there are no errors, return a message
// 	$data['success'] = true;
// 	$data['message'] = 'Success!';
// }



// Data from the edit camper form.
$id = strip_tags($_POST['id']);
$school = strip_tags($_POST['school']);
$email = strip_tags($_POST['email']);
$firstName = strip_tags($_POST['firstName']);
$lastName = strip_tags($_POST['lastName']);
$address1 = strip_tags($_POST['address1']);
$address2 = strip_tags($_POST['address2']);
$city = strip_tags($_POST['city']);
$state = strip_tags($_POST['state']);
$zip = strip_tags($_POST['zip']);
$phone = strip_tags($_POST['phone']);
$age = strip_tags($_POST['age']);
$gender = strip_tags($_POST['gender']);
$grade = strip_tags($_POST['grade']);
$tShirtSize = strip_tags($_POST['tShirtSize']);
$raceTime = strip_tags($_POST['raceTime']);
$dietaryRestrictions = strip_tags($_POST['dietaryRestrictions']);
$emergContName = strip_tags($_POST['emergContName']);
$emergContAddress = strip_tags($_POST['emergContAddress']);
$emergContPhone = strip_tags($_POST['emergContPhone']);
$emergContDr = strip_tags($_POST['emergContDr']);
$emergContDrPhone = strip_tags($_POST['emergContDrPhone']);
$emergContHealthInsuranceCarrier = strip_tags($_POST['emergContHealthInsuranceCarrier']);
$emergContHealthInsurancePolicy = strip_tags($_POST['emergContHealthInsurancePolicy']);
$emergContConditions = strip_tags($_POST['emergContConditions']);
$emergContAllergies = strip_tags($_POST['emergContAllergies']);
$emergContMedications = strip_tags($_POST['emergContMedications']);

function getCampers() {
	$sql = "SELECT * FROM camper ORDER BY id"; // example SELECT statement, Create my own.
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$campers = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($campers);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getCamperById($id) {
	$sql = "SELECT * FROM camper WHERE id = $id"; // example SELECT statement, Create my own.
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);
		$stmt->execute();		
		$camper = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($camper);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function deleteCamper($id) {
   
	$sql = "DELETE * FROM camper WHERE id = $id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->execute();
		$db = null;
		echo true;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
	
}

function editCamper($id) {
	$sql = "UPDATE camper 
		SET school=$school, email=$email, firstName=$firstName, lastName=$lastName, address1=$address1, address2=$address2, city=$city, state=$state, zip=$zip, phone=$phone, age=$age, gender=$gender, grade=$grade, tShirtSize=$tShirtSize, raceTime=$raceTime, dietaryRestrictions=$dietaryRestrictions, emergContName=$emergContName, emergContAddress=$emergContAddress, emergContPhone=$emergContPhone, emergContDr=$emergContDr, emergContDrPhone=$emergContDrPhone, emergContHealthInsuranceCarrier=$emergContHealthInsuranceCarrier, emergContHealthInsurancePolicy=$emergContHealthInsurancePolicy, emergContConditions=$emergContConditions, emergContAllergies=$emergContAllergies, emergContMedications=$emergContMedications 
		WHERE id = $id";
	try {
		$db = getDB();
		$stmt = $db->prepare($sql);  
		$stmt->execute();
		$db = null;
		echo true;
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getAdmin() {
	$sql = "SELECT * FROM user ORDER BY id"; // example SELECT statement, Create my own.
	try {
		$db = getDB();
		$stmt = $db->query($sql);  
		$admin = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		echo json_encode($admin);
	} catch(PDOException $e) {
	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}






// function getUserUpdates() {
// 	$sql = "SELECT A.user_id, A.username, A.name, A.profile_pic, B.update_id, B.user_update, B.created FROM users A, updates B WHERE A.user_id=B.user_id_fk  ORDER BY B.update_id DESC";
// 	try {
// 		$db = getDB();
// 		$stmt = $db->prepare($sql); 
// 		$stmt->execute();		
// 		$updates = $stmt->fetchAll(PDO::FETCH_OBJ);
// 		$db = null;
// 		echo '{"updates": ' . json_encode($updates) . '}';
		
// 	} catch(PDOException $e) {
// 	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
// 		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
// 	}
// }

// function getUserUpdate($update_id) {
// 	$sql = "SELECT A.user_id, A.username, A.name, A.profile_pic, B.update_id, B.user_update, B.created FROM users A, updates B WHERE A.user_id=B.user_id_fk AND B.update_id=:update_id";
// 	try {
// 		$db = getDB();
// 		$stmt = $db->prepare($sql);
//         $stmt->bindParam("update_id", $update_id);		
// 		$stmt->execute();		
// 		$updates = $stmt->fetchAll(PDO::FETCH_OBJ);
// 		$db = null;
// 		echo '{"updates": ' . json_encode($updates) . '}';
		
// 	} catch(PDOException $e) {
// 	    //error_log($e->getMessage(), 3, '/var/tmp/php.log');
// 		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
// 	}
// }

// function insertUpdate() {
// 	$request = \Slim\Slim::getInstance()->request();
// 	$update = json_decode($request->getBody());
// 	$sql = "INSERT INTO updates (user_update, user_id_fk, created, ip) VALUES (:user_update, :user_id, :created, :ip)";
// 	try {
// 		$db = getDB();
// 		$stmt = $db->prepare($sql);  
// 		$stmt->bindParam("user_update", $update->user_update);
// 		$stmt->bindParam("user_id", $update->user_id);
// 		$time=time();
// 		$stmt->bindParam("created", $time);
// 		$ip=$_SERVER['REMOTE_ADDR'];
// 		$stmt->bindParam("ip", $ip);
// 		$stmt->execute();
// 		$update->id = $db->lastInsertId();
// 		$db = null;
// 		$update_id= $update->id;
// 		getUserUpdate($update_id);
// 	} catch(PDOException $e) {
// 		//error_log($e->getMessage(), 3, '/var/tmp/php.log');
// 		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
// 	}
// }

// function deleteUpdate($update_id) {
   
// 	$sql = "DELETE FROM updates WHERE update_id=:update_id";
// 	try {
// 		$db = getDB();
// 		$stmt = $db->prepare($sql);  
// 		$stmt->bindParam("update_id", $update_id);
// 		$stmt->execute();
// 		$db = null;
// 		echo true;
// 	} catch(PDOException $e) {
// 		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
// 	}
	
// }

// function getCamperSearch($query) {
// 	$sql = "SELECT user_id,username,name,profile_pic FROM users WHERE UPPER(name) LIKE :query ORDER BY user_id";
// 	try {
// 		$db = getDB();
// 		$stmt = $db->prepare($sql);
// 		$query = "%".$query."%";  
// 		$stmt->bindParam("query", $query);
// 		$stmt->execute();
// 		$campers = $stmt->fetchAll(PDO::FETCH_OBJ);
// 		$db = null;
// 		echo '{"campers": ' . json_encode($campers) . '}';
// 	} catch(PDOException $e) {
// 		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
// 	}
// }
?>