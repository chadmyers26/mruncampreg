<?php
namespace Drewm;

header('Access-Control-Allow-Origin: *');

require("http://www.myersrunningcamp.com/mrc_regform/vendor/autoload.php");
/**
 * Super-simple, minimum abstraction MailChimp API v2 wrapper
 * 
 * Uses curl if available, falls back to file_get_contents and HTTP stream.
 * This probably has more comments than code.
 *
 * Contributors:
 * Michael Minor <me@pixelbacon.com>
 * Lorna Jane Mitchell, github.com/lornajane
 * 
 * @author Drew McLellan <drew.mclellan@gmail.com> 
 * @version 1.1.1
 */
class MailChimp
{
    private $api_key;
    private $api_endpoint = 'https://us7.api.mailchimp.com/2.0';
    private $verify_ssl   = false;

    /**
     * Create a new instance
     * @param string $api_key Your MailChimp API key
     */
    function __construct($api_key)
    {
        $this->api_key = $api_key;
        list(, $datacentre) = explode('-', $this->api_key);
        $this->api_endpoint = str_replace('us7', $datacentre, $this->api_endpoint);
    }

    /**
     * Call an API method. Every request needs the API key, so that is added automatically -- you don't need to pass it in.
     * @param  string $method The API method to call, e.g. 'lists/list'
     * @param  array  $args   An array of arguments to pass to the method. Will be json-encoded for you.
     * @return array          Associative array of json decoded API response.
     */
    public function call($method, $args=array(), $timeout = 10)
    {
        return $this->makeRequest($method, $args, $timeout);
    }

    /**
     * Performs the underlying HTTP request. Not very exciting
     * @param  string $method The API method to be called
     * @param  array  $args   Assoc array of parameters to be passed
     * @return array          Assoc array of decoded result
     */
    private function makeRequest($method, $args=array(), $timeout = 10)
    {      
        $args['apikey'] = $this->api_key;

        $url = $this->api_endpoint.'/'.$method.'.json';

        if (function_exists('curl_init') && function_exists('curl_setopt')){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');       
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($args));
            $result = curl_exec($ch);
            curl_close($ch);
        } else {
            $json_data = json_encode($args);
            $result    = file_get_contents($url, null, stream_context_create(array(
                'http' => array(
                    'protocol_version' => 1.1,
                    'user_agent'       => 'PHP-MCAPI/2.0',
                    'method'           => 'POST',
                    'header'           => "Content-type: application/json\r\n".
                                          "Connection: close\r\n" .
                                          "Content-length: " . strlen($json_data) . "\r\n",
                    'content'          => $json_data,
                ),
            )));
        }

        return $result ? json_decode($result, true) : false;
    }
}

    $email = $_POST['email'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];

//subscribe a new registrant

    //$MailChimp = new \Drewm\MailChimp('3f78a64e4d47de2340763cac3c0eb89e-us8');
    //print_r($MailChimp->call('lists/list'));

/*
    $MailChimp = new \Drewm\MailChimp('3f78a64e4d47de2340763cac3c0eb89e-us8');
    $result = $MailChimp->call('lists/interest-groupings', array(
                    'id'                => '12bea8857d'
              ));
    print_r($result);
*/

    $MailChimp = new \Drewm\MailChimp('3f78a64e4d47de2340763cac3c0eb89e-us8');
    $result = $MailChimp->call('lists/subscribe', array(
                    'id'                => '12bea8857d',
                    'email'             => array('email'=>$email),
                    'merge_vars'        => array('FNAME'=>$firstName, 
                                              'LNAME'=>$lastName,
                                              'GROUPCHG'=>'Register',
                                                'GROUPINGS'=>array(
                                                  array(
                                                    'id'=>19937,'groups'=> array('Register')
                                                  )
                                                )),
                    'double_optin'      => false,
                    'update_existing'   => true,
                    'replace_interests' => false,
                    'send_welcome'      => false,
                ));

    // multiple recipients
    $to  = $email;

    // subject
    $subject = 'Myers Running Camp Registration';

    // message
    $message = '
    <html>
    <head>
      <title>Thank you for registering for the 2016 Myers Running Camp.</title>
    </head>
    <body>
      <p>Thank you for registering for the 2016 Myers Running Camp.</p>
      <table>
        <tr>
          <p>You have decided to attend a camp in which we feel very strongly about. It is our goals to give you a chance to hear from some world class speakers in their respective fields, demonstrate some training drills and techniques that are currently used by professional runners, and create an atmosphere where you will meet new people and go home with some new great friends.</p>
        </tr>
        <tr>
          <p>We will email you directions to the camp, and a list of things to bring about two weeks prior to camp. If you need to withdraw your registration, please keep in mind that you must do so BEFORE 3 weeks prior to the camp in order to get a refund minus a $50.00 processing fee. This is a pretty strict policy in order to ensure enough spots at the campgrounds.</p>
        </tr>
        <tr>
          <p>If you need to get in touch with us, please call Chad Myers at 614-581-2191. If you are paying with a check, please make it out to Myers Running Camp, and mail to: Myers Running Camp, 6595 Lake Rd. Pleasantville, OH 43148.</p>
        </tr>
      </table>
    </body>
    </html>
    ';

    // To send HTML mail, the Content-type header must be set
    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

    // Additional headers
    // Additional headers
    $headers .= 'To: ' . $to . "\r\n";
    $headers .= 'From: chadmyers262@gmail.com' . "\r\n" . "Reply-To: successive.testing@gmail.com" . "\r\n" .
"X-Mailer: PHP/" . phpversion();

    // Mail it
    mail($to, $subject, $message, $headers);
?>