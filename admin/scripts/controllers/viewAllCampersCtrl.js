'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:viewAllCampersCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('viewAllCampersCtrl', ['$scope', 'sCamperSvc', '_', '$location', function ($scope, sCamperSvc, _, $location) {

  	sCamperSvc.getAllCampers()
      .then(function(data) {
        $scope.allCampers = _.sortBy(data, 'lastName');
      });

    $scope.go = function ( path ) {
	  $location.path( path );
	}
	
}]);