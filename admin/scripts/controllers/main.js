'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('MainCtrl', ['$scope', 'sCamperSvc', '$location', function ($scope, sCamperSvc, $location) {

    sCamperSvc.getCampers()
      .then(function(data) {
        $scope.campers = data;
      });

    $scope.go = function ( path ) {
	  $location.path( path );
	}

}]);