'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:viewAllAdminCtrl
 * @description
 * # viewAllAdminCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('viewAllAdminCtrl', ['$scope', 'sCamperSvc', '_', function ($scope, sCamperSvc, _) {

	sCamperSvc.getAdmin()
      .then(function(data) {
        $scope.administrators = _.sortBy(data, 'lastName');
      }); 	

}]);