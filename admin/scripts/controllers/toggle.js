'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:toggleCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('toggleCtrl',['$scope', function($scope){
        $scope.custom = true;
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
        };
}]);