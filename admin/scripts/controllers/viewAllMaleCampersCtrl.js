'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:viewAllMaleCampersCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('viewAllMaleCampersCtrl', ['$scope', 'sCamperSvc', '_', '$location', function ($scope, sCamperSvc, _, $location) {

  	sCamperSvc.getAllMaleCampers()
      .then(function(data) {
        $scope.allMaleCampers = _.sortBy(data, 'lastName');
      });

    $scope.go = function ( path ) {
	  $location.path( path );
	}

}]);