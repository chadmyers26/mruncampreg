'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:viewAllFemaleCampersCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('viewAllFemaleCampersCtrl', ['$scope', 'sCamperSvc', '_', '$location', function ($scope, sCamperSvc, _, $location) {

  	sCamperSvc.getAllFemaleCampers()
      .then(function(data) {
        $scope.allFemaleCampers = _.sortBy(data, 'lastName');
      });

    $scope.go = function ( path ) {
	  $location.path( path );
	}

}]);