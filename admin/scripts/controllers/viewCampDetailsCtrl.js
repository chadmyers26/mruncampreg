'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:viewCampDetailsCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
myersCampRegistrationApp.controller('viewCampDetailsCtrl', ['$scope', 'sCamperSvc', '_', function ($scope, sCamperSvc, _) {

  	sCamperSvc.getCampers()
      .then(function(data) {
        $scope.campdetails = data;
      });
	
}]);