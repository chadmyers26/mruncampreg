'use strict';

/**
 * @ngdoc overview
 * @name myersCampRegistrationApp
 * @description
 * # myersCampRegistrationApp
 *
 * Main module of the application.
 */
var myersCampRegistrationApp = angular.module('myersCampRegistrationApp', ['ngRoute', 'ngTouch'])
  .constant('_', _)
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/viewAllCampers', {
        templateUrl: 'views/viewAllCampers.html',
        controller: 'viewAllCampersCtrl',
        controllerAs: 'viewAllCampers'
      })
      .when('/viewAllAdmin', {
        templateUrl: 'views/viewAllAdmin.html',
        controller: 'viewAllAdminCtrl',
        controllerAs: 'viewAllAdmin'
      })
      .when('/editCamper/:id', {
        templateUrl: 'views/camperDetail.html',
        controller: 'editCamperCtrl',
        controllerAs: 'editCamper'
      })
      .when('/viewAllMaleCampers', {
        templateUrl: 'views/viewAllMaleCampers.html',
        controller: 'viewAllMaleCampersCtrl',
        controllerAs: 'viewAllMaleCampers'
      })
      .when('/viewAllFemaleCampers', {
        templateUrl: 'views/viewAllFemaleCampers.html',
        controller: 'viewAllFemaleCampersCtrl',
        controllerAs: 'viewAllFemaleCampers'
      })
      .when('/viewCampDetails', {
        templateUrl: 'views/campDetails.html',
        controller: 'viewCampDetailsCtrl',
        controllerAs: 'viewCampDetailsCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
