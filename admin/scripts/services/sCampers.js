'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.service:sCamperSvc
 * @description
 * # sCamperSvc
 * Camper Service of the myersCampRegistrationApp
 */
myersCampRegistrationApp.factory('sCamperSvc', ['$http', '_', '$q', function ($http, _, $q) {

  	var baseUrl = "http://myersrunningcamp.com/mrc_regform/serverside/api",
		formatCamperData = function(data) {
			var totalCampers = data.length,
	        	totalFemaleCampers = _.where(data, { 'gender': 'Female' }),
	        	totalMaleCampers = _.where(data, { 'gender': 'Male' }),
	        	earlyTeam = _.where(data, { 'registeringAs': 'Early Team Registrant' }),
	        	lateTeam = _.where(data, { 'registeringAs': 'Late Team Registrant' }),
	        	earlyIndiv = _.where(data, { 'registeringAs': 'Early Individual' }),
	        	lateIndiv = _.where(data, { 'registeringAs': 'Late Individual Registrant' }),
	        	paidOnline = _.where(data, { 'paymentType': 'Online' }),
	        	payByCheck = _.where(data, { 'paymentType': 'Check' });

			var campers = { 
			    totalCampers: totalCampers, 
			    totalFemaleCampers: totalFemaleCampers.length, 
			    totalMaleCampers: totalMaleCampers.length,
			    camperTypes: { 
			    	earlyTeam : earlyTeam.length,
			    	lateTeam : lateTeam.length,
			    	earlyIndiv : earlyIndiv.length,
			    	lateIndiv : lateIndiv.length
			    },
			    paidOnline: paidOnline.length,
			    payByCheck: payByCheck.length,
			    campData: {
			    	tAmountEarlyIndv: (earlyIndiv.length * 270.00).toFixed(2),
			    	tAmountEarlyTeam: (earlyTeam.length * 250.00).toFixed(2),
			    	tAmountLateIndv: (lateIndiv.length * 295.00).toFixed(2),
			    	tAmountLateTeam: (lateTeam.length * 275.00).toFixed(2),
			    	totalIncomeFromCampers: (( parseInt((earlyIndiv.length * 270.00).toFixed(2)) + parseInt((earlyTeam.length * 250.00).toFixed(2)) + parseInt((lateIndiv.length * 295.00).toFixed(2)) + parseInt((lateTeam.length * 275.00).toFixed(2)) ) + parseInt(500)).toFixed(2),
			    	expenses: {
			    		lodging: ( parseInt(3) * ( parseInt(totalCampers) + 7 ) + parseInt(16) ).toFixed(2),
			    		lodgingQuantity: ( parseInt(totalCampers) + 7 ),
			    		lodgingCosts: 16.00,
			    		foodCosts: 600.00,
			    		diner: ( parseInt(3) * ( parseInt(totalCampers) + 7 ) + parseInt(7) ).toFixed(2),
			    		dinerQuantity: ( parseInt(totalCampers) + 7 ),
			    		dinerCosts: 7.00,
			    		gatorade: 156.00,
			    		speakers: ( parseInt(1) + parseInt(200) ).toFixed(2), 
			    		waterBottles: ( parseInt(1) * ( parseInt(totalCampers) + 40 ) + parseInt(1.60) ).toFixed(2),
			    		wBottleQuantity: ( parseInt(totalCampers) + 40 ),
			    		wBottleCosts: 1.60,
			    		tShirts: 0.00,
			    		pool: 640.00,
			    		roadSigns: 70.00,
			    		supplies: 0.00
			    	},
			    	salaries: {
			    		headCounselorSlry: 500.00
			    	}
			    }
			};

			var totalExpensesArray = [
				campers.campData.expenses.lodging,
				campers.campData.expenses.foodCosts,
				parseInt(campers.campData.expenses.diner),
				campers.campData.expenses.gatorade,
				parseInt(campers.campData.expenses.speakers),
				parseInt(campers.campData.expenses.waterBottles),
				campers.campData.expenses.tShirts,
				campers.campData.expenses.pool,
				campers.campData.expenses.roadSigns,
				campers.campData.expenses.supplies,
			];

			campers.campData.expenses.totalExpenses = _.sum(totalExpensesArray).toFixed(2);
			campers.campData.salaries.totalSalaries = campers.campData.salaries.headCounselorSlry;
			campers.campData.totalOverallIncome = ( parseInt(campers.campData.totalIncomeFromCampers) - parseInt(campers.campData.expenses.totalExpenses) - parseInt(campers.campData.salaries.totalSalaries) );

			return campers;
  		},

  		formatFemaleCamperData = function(data) {
			var femaleCampers = _.where(data, { 'gender': 'Female' });

			return femaleCampers;
  		},

  		formatMaleCamperData = function(data) {
			var maleCampers = _.where(data, { 'gender': 'Male' });

			return maleCampers;
  		};

  	return {
	  	getCampers: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/campers";

	  		$http.get(url)
	  			.success(function(data) {
					var campersData = formatCamperData(data);
	  				deferred.resolve(campersData);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    getAllCampers: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/campers";

	  		$http.get(url)
	  			.success(function(data) {
	  				deferred.resolve(data);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    getAllFemaleCampers: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/campers";

	  		$http.get(url)
	  			.success(function(data) {
					var femaleCampers = formatFemaleCamperData(data);
	  				deferred.resolve(femaleCampers);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    getAllMaleCampers: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/campers";

	  		$http.get(url)
	  			.success(function(data) {
					var maleCampers = formatMaleCamperData(data);
	  				deferred.resolve(maleCampers);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    getCamperByID: function (id) {
	    	var deferred = $q.defer();
	    	var url = baseUrl + "/" + id;

	    	$http.get(url)
	  			.success(function(data) {
	  				deferred.resolve(data);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    editCamperByID: function (formData) {
	    	console.log(formData);

	    	var deferred = $q.defer();
	    	var url = baseUrl + "/campers/edit/" + id;

	    	$http.post(url, formData)
				.success(function(data) {
					// if (!data.success) {
					// 	// if not successful, bind errors to error variables
					// 	$scope.errorName = data.errors.name;
					// 	$scope.errorSuperhero = data.errors.superheroAlias;
					// } else {
					// 	// if successful, bind success message to message
					// 	$scope.message = data.message;
					// }
					deferred.resolve(data);
				})
				.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    }

	  //   $http({
			// 	method  : 'POST',
			// 	url     : url,
			// 	data    : formData,  // pass in data as strings
			// 	headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
			// })
			// .success(function(data) {
			// 	deferred.resolve(data);
			// })
			// .error(function(reason) {
  	// 			deferred.reject(reason);
  	// 		});

  	// 		return deferred.promise;

	    // deleteCamperByID: function (id) {
	    // 	var deferred = $q.defer();
	  		// var url = baseUrl + "/campers/delete/:id";


	    // }
  	};

}]);