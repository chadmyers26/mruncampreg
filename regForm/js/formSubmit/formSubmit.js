$( document ).ready(function() {

	$( "#registrationForm" ).submit(function() {
    	event.preventDefault();

    	var regValues = $("#registrationForm").serialize(),
    		firstName = $("#firstName").val(),
    		lastName = $("#lastName").val(),
    		email = $("#email").val(),
    		paymentType = $("#paymentType").val();

  		$.ajax({
		  method: "POST",
		  url: "../serverside/submitCamper.php",
		  data: regValues,
		  success: function(data) {
	  		$.ajax({
			  type: "POST",
			  url: "http://www.myersrunningcamp.com/mrc_regform/serverside/mailChimp.php",
			  data: {'email':email, 'firstName':firstName, 'lastName':lastName},
			  success: function (data) {
			  	console.log(data);
			  }
			});
		  }
		})
		  .done(function( msg ) {

		  	if (paymentType == "Online") {
		    	$("#registrationConfirm").show();
		    	$("#registrationSubmit").attr("disabled", true);
		  	} else {
		  		$("#registrationConfirmCheck").show();
		  		$("#registrationSubmit").attr("disabled", true);
		  	}
		  });
	});
});