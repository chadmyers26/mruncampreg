$( document ).ready(function() {
	var registeringAsChosen,
		paymentTypeChosen;

    // onchange of registering as input type, show payment dropdown
    $("#registeringAs").change(function() {
    	registeringAsChosen = $('#registeringAs').val();
    	
    	if (registeringAsChosen == "") {
    		$("#paymentTypeDiv, #payByCheck").hide();
        	$("#registrationSubmit").attr("disabled", true);
        	$('#paymentType').prop('selectedIndex',0);
    	} else {
    		$("#paymentTypeDiv").show();
    	}
    }); 

    $('#paymentType').on('change', function(e) {
        // if selected type is "online", then show online forms. If "check" then show info about paying by check
        var paymentTypeChosen = $('#paymentType').val();

        if (paymentTypeChosen == "Online") {
            $("#payByCheck").hide();
            $("#payOnline").show();
            $("#registrationSubmit").attr("disabled", false);
        } else if (paymentTypeChosen == "Check") {
            $("#payByCheck").show();
            $("#payOnline").hide();
            $("#registrationSubmit").attr("disabled", false);
        }
    });
});