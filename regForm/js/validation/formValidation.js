$( document ).ready(function() {

	//regex's

	var _emailRegEx = /^[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,})$/,
		_telephoneNumberRegEx = /^(?:\+?1[-. ]?)?\(?([2-9][0-9][0-9])\)?[-. ]?([2-9][0-9]{2})[-. ]?([0-9]{4})$/,
		_numRegEx = /^[0-9]+$/i,
		_addressRegEx = /^[a-z0-9\-\#., \/]+$/i,
		_nameRegEx = /^[a-z \-\']+$/i,
		_usZipRegEx = /^\d{5}(-\d{4})?$/,
		_creditCardRegEx = /^(?:4[0-9]{12}(?:[0-9]{3})|5[1-5][0-9]{14}|6(?:011|2[4-6][0-9]|28[2-8]|4[4-9][0-9]|5[0-9][0-9])[0-9]{12}|3(?:0[0-5][0-9]{13}|095[0-9]{12}|6[0-9]{12}|[89][0-9]{14})|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|3088|1800|35\d{3})\d{11})|(2[5-9][0-9]|58[0-9]|59[0-9]|81[3-9]|82[0-9]|8[6-9][0-9]|9[6-7][0-9]){1}\d{6,13}|(?:x+)\d{4}$/,
		_cardTypeRegEx = /^[ADJMUVZ]$/,
		_raceTime = /^(1?[0-9]|2[0-3]):[0-5][0-9]$/,
		_errorMsg = "<p class='errorMsg'>Please correct the highlighted fields.</p>",
		_invalidMsg = "Please Enter a Valid ",
		currentInputField,
		currentInputValue,
		errors = true;

    // show error message
    var showErrorMsg = function(fieldName) {
    	$("#" + fieldName).removeClass("error");
    	$("#" + fieldName).removeClass("success");
    	$("#" + fieldName).parent().find(".errorMsg").hide();
    	$("#" + fieldName).before(_errorMsg);
    	$("#" + fieldName).addClass("error");
    	errors = true;
    },

    _showError = function() {
    	$("#registrationForm .errorMsg").html("<p>Please make sure you have completed all the required fields.</p>").css("display", "block");
    	window.scrollTo(0,0);
    },

    // hide error message
    hideErrorMsg = function(fieldName) {
		$("#" + fieldName).removeClass("error");
		$("#" + fieldName).addClass("success");
    	$("#" + fieldName).parent().find(".errorMsg").hide();
    },

    showValidated = function(fieldName) {
		$("#" + fieldName).addClass("success");
		hideErrorMsg(fieldName);
		errors = false;
    },

    checkAge = function(fieldName) {
    	var camperStatedAge = $("#age").val();
    	if (camperStatedAge < "18") {
    		return false;
    	} else {
    		return true;
    	}
    }

    validateInputField = function(fieldName, fieldValue) {
    	switch(fieldName) {
		    case "school":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		    case "email":
		        if ( _emailRegEx.test(fieldValue) && fieldValue != "") {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "firstName":
		        if ( fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "lastName":
		        if ( fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "address1":
		        if ( _addressRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "city":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		    case "zip":
		        if ( _usZipRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "phone":
		        if ( _telephoneNumberRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "age":
		        if ( _numRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "raceTime":
		        if ( fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "dietaryRestrictions":
		    	if ( fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "emergContName":
		        if ( fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "emergContAddress":
		        if ( _addressRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "emergContPhone":
		        if ( _telephoneNumberRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "emergContDr":
		        if ( fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "emergContDrPhone":
		        if ( _telephoneNumberRegEx.test(fieldValue) && fieldValue != "" ) {
		        	showValidated(fieldName);
		        } else {
		        	showErrorMsg(fieldName);
		        }
		        break;
		    case "emergContHealthInsuranceCarrier":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		    case "emergContHealthInsurancePolicy":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		    case "emergContConditions":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		    case "emergContAllergies":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		    case "emergContMedications":
		        if (fieldValue == "") {
		        	showErrorMsg(fieldName);
		        } else {
		        	showValidated(fieldName);
		        }
		        break;
		}

    };

    // on keyup, get the input in focus and validate it
    $( "input" ).blur(function() {
    	currentInputField = $(this).attr('id');
    	currentInputValue = $(this).val();

    	validateInputField(currentInputField, currentInputValue);
	});

	$( "textarea" ).blur(function() {
    	currentInputField = $(this).attr('id');
    	currentInputValue = $(this).val();

    	validateInputField(currentInputField, currentInputValue);
	});

	$( "input[value='Camper']" ).click(function() {
		var ageStated = $("#age").val(),
			ageValidation = checkAge(ageStated);
		if (!ageValidation) {
    		$("input[value='Camper']").prop("checked", false);
    		$("input[value='LegalGuardian']").prop("checked", true);
    		$("#emergContConsentName").show();
    		$("label[for='emergContConsentName']").show();
    		$("input[value='Camper']").parent().prepend("<p class='errorMsg'>Based on the age you stated you are, you must have a guardian's permission</p>");
    	} else {
    		$("input[value='Camper']").prop("checked", true);
    		$("input[value='LegalGuardian']").prop("checked", false);
    		$("#emergContConsentName").hide();
    		$("label[for='emergContConsentName']").hide();
    		$("input[value='Camper']").parent().find(".errorMsg").remove();
    	}
	});

	$( "input[value='LegalGuardian']" ).click(function() {
		$("input[value='Camper']").parent().find(".errorMsg").remove();
		$("#emergContConsentName").show();
		$("label[for='emergContConsentName']").show();
	});

	$( "#registrationForm" ).submit(function() {
    	event.preventDefault();

    	if (!errors) {
	    	var regValues = $("#registrationForm").serialize(),
	    		firstName = $("#firstName").val(),
	    		lastName = $("#lastName").val(),
	    		email = $("#email").val(),
	    		paymentType = $("#paymentType").val();

	  		$.ajax({
			  method: "POST",
			  url: "../serverside/submitCamper.php",
			  data: regValues,
			  success: function(data) {
		  		$.ajax({
				  type: "POST",
				  url: "http://www.myersrunningcamp.com/mrc_regform/serverside/mailChimp.php",
				  data: {'email':email, 'firstName':firstName, 'lastName':lastName},
				  success: function (data) {
				  	console.log(data);
				  }
				});
			  }
			})
			  .done(function( msg ) {

			  	if (paymentType == "Online") {
			    	$("#registrationConfirm").show();
			    	$("#registrationSubmit").attr("disabled", true);
			  	} else {
			  		$("#registrationConfirmCheck").show();
			  		$("#registrationSubmit").attr("disabled", true);
			  	}
			  });
    	} else {
    		_showError();
    	}
    	
	});
});