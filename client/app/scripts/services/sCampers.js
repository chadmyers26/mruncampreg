'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.service:sCamperSvc
 * @description
 * # sCamperSvc
 * Camper Service of the myersCampRegistrationApp
 */
angular.module('myersCampRegistrationApp').factory('sCamperSvc', ['$http', '_', '$q', function ($http, _, $q) {

  	var baseUrl = "http://localhost:8888/mruncampreg/serverside/api",
		formatCamperData = function(data) {
			var totalCampers = data.length,
	        	totalFemaleCampers = _.where(data, { 'gender': 'Female' }),
	        	totalMaleCampers = _.where(data, { 'gender': 'Male' }),
	        	earlyTeam = _.where(data, { 'registeringAs': 'Early Team Registrant' }),
	        	lateTeam = _.where(data, { 'registeringAs': 'Late Team Registrant' }),
	        	earlyIndiv = _.where(data, { 'registeringAs': 'Early Individual' }),
	        	lateIndiv = _.where(data, { 'registeringAs': 'Late Individual Registrant' });

			var campers = { 
			    totalCampers: totalCampers, 
			    totalFemaleCampers: totalFemaleCampers.length, 
			    totalMaleCampers: totalMaleCampers.length,
				    camperTypes: { 
				    	earlyTeam : earlyTeam.length,
				    	lateTeam : lateTeam.length,
				    	earlyIndiv : earlyIndiv.length,
				    	lateIndiv : lateIndiv.length
				    }
			};

			return campers;
  		};

  	return {
	  	getCampers: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/campers";

	  		$http.get(url)
	  			.success(function(data) {
					var campersData = formatCamperData(data);
	  				deferred.resolve(campersData);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    getAllCampers: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/campers";

	  		$http.get(url)
	  			.success(function(data) {
	  				deferred.resolve(data);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    getCamperByID: function (id) {
	    	var deferred = $q.defer();
	    	var url = baseUrl + "/" + id;

	    	$http.get(url)
	  			.success(function(data) {
	  				deferred.resolve(data);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    },

	    editCamperByID: function (formData) {
	    	console.log(formData);

	    	var deferred = $q.defer();
	    	var url = baseUrl + "/campers/edit/" + id;

	    	$http.post(url, formData)
				.success(function(data) {
					// if (!data.success) {
					// 	// if not successful, bind errors to error variables
					// 	$scope.errorName = data.errors.name;
					// 	$scope.errorSuperhero = data.errors.superheroAlias;
					// } else {
					// 	// if successful, bind success message to message
					// 	$scope.message = data.message;
					// }
					deferred.resolve(data);
				})
				.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    }

	  //   $http({
			// 	method  : 'POST',
			// 	url     : url,
			// 	data    : formData,  // pass in data as strings
			// 	headers : { 'Content-Type': 'application/x-www-form-urlencoded' }  // set the headers so angular passing info as form data (not request payload)
			// })
			// .success(function(data) {
			// 	deferred.resolve(data);
			// })
			// .error(function(reason) {
  	// 			deferred.reject(reason);
  	// 		});

  	// 		return deferred.promise;

	    // deleteCamperByID: function (id) {
	    // 	var deferred = $q.defer();
	  		// var url = baseUrl + "/campers/delete/:id";


	    // }
  	};

}]);