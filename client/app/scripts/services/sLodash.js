'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.service:lodash
 * @description
 * # lodash
 * Controller of the myersCampRegistrationApp
 */
angular.module('myersCampRegistrationApp').factory('_', [function ($window) {
	var _ = $window._;

	delete( $window._ );

	return ( _ );

}]);