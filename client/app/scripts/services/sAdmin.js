'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.service:sAdminSvc
 * @description
 * # sAdminSvc
 * Admin service of the myersCampRegistrationApp
 */
angular.module('myersCampRegistrationApp').factory('sAdminSvc', ['$http', '_', '$q', function ($http, _, $q) {

  	var baseUrl = "http://localhost:8888/mruncampreg/serverside/api";

  	return {
	    getAdmin: function () {
	  		var deferred = $q.defer();
	  		var url = baseUrl + "/getAdmin";

	  		$http.get(url)
	  			.success(function(data) {
	  				deferred.resolve(data);
	  			})
	  			.error(function(reason) {
	  				deferred.reject(reason);
	  			});

  			return deferred.promise;
	    }
  	};

}]);