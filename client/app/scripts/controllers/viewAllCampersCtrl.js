'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:viewAllCampersCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
angular.module('myersCampRegistrationApp')
  .controller('viewAllCampersCtrl', ['$scope', 'sCamperSvc', '_', function ($scope, sCamperSvc, _) {

  	sCamperSvc.getAllCampers()
      .then(function(data) {
        $scope.allCampers = _.sortBy(data, 'lastName');
      });

}]);