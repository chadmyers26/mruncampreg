'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the myersCampRegistrationApp
 */
angular.module('myersCampRegistrationApp')
  .controller('MainCtrl', ['$scope', 'sCamperSvc', function ($scope, sCamperSvc) {

    sCamperSvc.getCampers()
      .then(function(data) {
        $scope.campers = data;
      });

}]);