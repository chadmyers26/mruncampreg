'use strict';

/**
 * @ngdoc function
 * @name myersCampRegistrationApp.controller:editCamperCtrl
 * @description
 * # editCamperCtrl
 * Controller of the myersCampRegistrationApp
 */
angular.module('myersCampRegistrationApp')
  .controller('editCamperCtrl', ['$scope', 'sCamperSvc', '_', function ($scope, sCamperSvc, _) {

  	var id = window.location.hash,
  		$scope.camper = {};

	sCamperSvc.getCamperByID(id)
      .then(function(data) {
        $scope.camper = data;
      });

    $scope.processForm = function() {
    	$scope.camper;

    	sCamperSvc.editCamperByID($scope.camper)
    		.then(function(data) {
    			$scope.camper = data;
    			console.log($scope.camper);
    		});
	};

}]);