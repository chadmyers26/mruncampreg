'use strict';

/**
 * @ngdoc overview
 * @name myersCampRegistrationApp
 * @description
 * # myersCampRegistrationApp
 *
 * Main module of the application.
 */
angular
  .module('myersCampRegistrationApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .constant('_', _)
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/viewAllCampers', {
        templateUrl: 'views/viewAllCampers.html',
        controller: 'viewAllCampersCtrl',
        controllerAs: 'viewAllCampers'
      })
      .when('/viewAllAdmin', {
        templateUrl: 'views/viewAllAdmin.html',
        controller: 'viewAllAdminCtrl',
        controllerAs: 'viewAllAdmin'
      })
      .when('/editCamper/:id', {
        templateUrl: 'views/camperDetail.html',
        controller: 'editCamperCtrl',
        controllerAs: 'editCamper'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
